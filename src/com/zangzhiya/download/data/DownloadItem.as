/*
 * Author:	Zhiya Zang
 * Email:	zangzhiya@gmail.com
 * Date:	2013-04-19
 * Desc:	Download item datastruct
 */

package com.zangzhiya.download.data
{
	public class DownloadItem
	{
		private var _id:String;
		private var _url:String;
		private var _path:String;
		private var _status:uint;
		
		public static const STATUS_WAIT:uint = 0;
		public static const STATUS_COMPLETE:uint = 1;
		public static const STATUS_PROGRESS:uint = 2;
		
		public function DownloadItem(id:String, url:String, path:String, status:uint = STATUS_WAIT)
		{
			_id = id;
			_url = url;
			_path = path;
			_status = status;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get url():String
		{
			return _url;
		}
		
		public function get path():String
		{
			return _path;
		}
		
		public function get status():uint
		{
			return _status;
		}
		
		public function set id(id:String):void
		{
			_id = id;
		}
		
		public function set url(url:String):void
		{
			_url = url;
		}
		
		public function set path(path:String):void
		{
			_path = path;
		}
		
		public function set status(status:uint):void
		{
			_status = status;
		}
	}
}
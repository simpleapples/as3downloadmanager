/*
 * Author:	Zhiya Zang
 * Email:	zangzhiya@gmail.com
 * Date:	2013-04-19
 * Desc:	Download Manager
 */

package com.zangzhiya.download
{
	import com.duobei.download.event.DownloadEvent;
	import com.duobei.download.service.DownloadService;
	import com.zangzhiya.download.data.DownloadItem;
	import com.zangzhiya.download.event.DownloadEvent;
	import com.zangzhiya.download.service.DownloadService;
	
	import flash.events.Event;
	import flash.filesystem.File;
	
	public class DownloadManager
	{	
		private static var _instance:DownloadManager;
		
		private var _downloadList:Array;
		private var _downloadProcess:DownloadService;
		
		private var _isDownloading:Boolean = false;
		
		public static function get instance():DownloadManager {
			
			if( _instance == null ) {
				_instance = new DownloadManager( new SingletonEnforcer() );
			}
			
			return _instance;
		}
		
		public function DownloadManager(singletonEnforcer:SingletonEnforcer)
		{
			init();
		}
		
		private function init():void
		{
			_downloadList = new Array();
		}
		
		public function addItem(url:String, path:String):void
		{
			var item:DownloadItem = new DownloadItem(_downloadList.length.toString(), url, path);
			_downloadList.push(item);
			checkStatus();
		}
		
		public function removeItem(id:uint):void
		{
			var i:uint;
			for (i = 0; i < _downloadList.length; i++) {
				if (_downloadList[i].id == id) {
					_downloadList.slice(i, 1);
					break;
				}
			}
		}
		
		private function downloadItem(url:String, path:String):void
		{
			_downloadProcess = new DownloadService(url);
			_downloadProcess.addEventListener(DownloadEvent.DOWNLOAD_COMPLETE, downloadCompleteHandler);
			_downloadProcess.addEventListener(DownloadEvent.DOWNLOAD_ERROR, downloadErrorHandler);
			if (checkFileExist(path + "/" + getFileName(url))) {
				_downloadProcess.dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_COMPLETE));
			} else {
				_downloadProcess.download(path);
			}
		}
		
		protected function downloadCompleteHandler(event:Event):void
		{
			var i:uint;
			for (i = 0; i < _downloadList.length; i++) {
				if (_downloadList[i].status == DownloadItem.STATUS_PROGRESS) {
					_downloadList[i].status = DownloadItem.STATUS_COMPLETE;
					_isDownloading = false;
					break;
				}
			}
			checkStatus();
		}
		
		protected function downloadErrorHandler(event:Event):void
		{
			var i:uint;
			for (i = 0; i < _downloadList.length; i++) {
				if (_downloadList[i].status == DownloadItem.STATUS_PROGRESS) {
					_downloadList[i].status = DownloadItem.STATUS_WAIT;
					_isDownloading = false;
					break;
				}
			}
			checkStatus();
		}
		
		private function checkStatus():void
		{
			var i:uint;
			if (_isDownloading == false) {
				for (i = 0; i < _downloadList.length; i++) {
					if (_downloadList[i].status == DownloadItem.STATUS_WAIT) {
						_downloadList[i].status = DownloadItem.STATUS_PROGRESS;
						_isDownloading = true;
						downloadItem(_downloadList[i].url, _downloadList[i].path);
						break;
					}
				}
			}
		}
		
		private function checkFileExist(path:String):Boolean
		{
			var file:File = new File(path);
			return file.exists;
		}
		
		private function getFileName(path:String):String
		{
			// 查找最后一个"/"
			var flag:int = path.lastIndexOf("/");
			// 截取文件名
			var fileName:String = path.substr(flag + 1, path.length);
			return fileName;
		}
	}
}

internal class SingletonEnforcer{}
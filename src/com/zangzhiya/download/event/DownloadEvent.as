package com.zangzhiya.download.event
{
	import flash.events.Event;
	
	public class DownloadEvent extends Event
	{
		public static const DOWNLOAD_COMPLETE:String = "DOWNLOAD_COMPLETE";
		public static const DOWNLOAD_ERROR:String = "DOWNLOAD_ERROR";
				
		public function DownloadEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}

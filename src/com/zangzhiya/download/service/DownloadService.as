/*
 * Author:	Zhiya Zang
 * Email:	zangzhiya@gmail.com
 * Date:	2013-04-19
 * Desc:	Download service
 */

package com.zangzhiya.download.service
{
	import com.zangzhiya.download.event.DownloadEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;

	public class DownloadService extends Sprite
	{
		private var _url:String;
		private var _path:String;
		
		public function DownloadService(url:String)
		{
			_url = url;	
		}
		
		public function download(path:String):void
		{
			_path = path;
			
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			
			loader.addEventListener(Event.COMPLETE, completeHandler);
			loader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loadError);
			
			loader.load(new URLRequest(_url));
		}
		
		private function progressHandler(e:ProgressEvent):void
		{
			if (e.bytesTotal > 0) {
				trace("已下载 " + Math.round(100 * e.bytesLoaded / e.bytesTotal) + "%");
			}
		}
		
		private function loadError(e:IOErrorEvent):void
		{
			this.dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_ERROR));
			trace("Download Error");
		}
		
		private function completeHandler(e:Event):void
		{
			var localFile:LocalFileService = new LocalFileService();
			var fileName:String = getFileName(_url);
			_path = _path + "/" + fileName;
			localFile.writeAppFile(_path, e.target.data);
			this.dispatchEvent(new DownloadEvent(DownloadEvent.DOWNLOAD_COMPLETE));
			trace("Download Complete");
		}
		
		private function getFileName(path:String):String
		{
			// 查找最后一个"/"
			var flag:int = path.lastIndexOf("/");
			// 截取文件名
			var fileName:String = path.substr(flag + 1, path.length);
			return fileName;
		}
	}
}
/*
 * Author:	Zhiya Zang
 * Email:	zangzhiya@gmail.com
 * Date:	2013-04-19
 * Desc:	Read file from local path and write file to local path
 */

package com.zangzhiya.download.service
{	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;

	public class LocalFileService
	{
		public function LocalFileService()
		{
		}
		
		public function writeAppFile(path:String, fileData:ByteArray):void
		{
			var file:File = new File(path);
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.position = 0;
			fileStream.writeBytes(fileData);
			fileStream.close();
		}
		
		public function readAppFile(path:String):ByteArray 
		{
			var file:File = new File(path);
			var fileStream:FileStream = new FileStream();
			var data:ByteArray = new ByteArray();
			fileStream.open(file, FileMode.READ);
			fileStream.position = 0;
			fileStream.readBytes(data);
			fileStream.close();
			return data;
		}
		
		public function checkFileExist(path:String):Boolean
		{
			var file:File = new File(path);
			return file.exists;
		}
	}
}
AS3DownloadManager
==================

简介
-----
**AS3DownloadManager**, Actionscript 3 下载管理器。

版本
-----
* 0.0.3 修复单个文件下载逻辑错误
* 0.0.2 如果文件存在则不下载
* 0.0.1 全局下载列表，手动开启下载

